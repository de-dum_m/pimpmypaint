/*
** paint.h for includes in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Jun 21 20:19:35 2014 de-dum_m
** Last update Mon Jun 23 13:46:13 2014 de-dum_m
*/

#ifndef PAINT_H_
# define PAINT_H_

# define SUCCESS	0
# define FAILURE	-1

# define X_PWIN		600
# define Y_PWIN		600
# define X_CWIN		160
# define Y_CWIN		80
# define X_TWIN		68
# define Y_TWIN		192

# define DEFAULT_SIZE	150

# define P_SQUARE	0
# define P_CIRCLE	1
# define P_TRIANGLE	2
# define P_LINE		3

# define CBLACK		0x000000
# define CBLUE		0x0000FF
# define CRED		0xFF0000
# define CGREEN		0x00FF00
# define CWHITE		0xFFFFFF
# define CMAGENTA	0xFF00FF
# define CCYAN		0x00FFFF
# define CYELLOW	0xFFFF00

# define ICO_FOLDER	"icons/"
# define CIRCLE_SEL_ICO	ICO_FOLDER"Circle_sel.xpm"
# define CIRCLE_ICO	ICO_FOLDER"Circle.xpm"
# define TRIAN_SEL_ICO	ICO_FOLDER"Triangle_sel.xpm"
# define TRIAN_ICO	ICO_FOLDER"Triangle.xpm"
# define SQUARE_SEL_ICO	ICO_FOLDER"Square_sel.xpm"
# define SQUARE_ICO	ICO_FOLDER"Square.xpm"
# define LINE_SEL_ICO	ICO_FOLDER"Line_sel.xpm"
# define LINE_ICO	ICO_FOLDER"Line.xpm"
# define FILL_SEL_ICO	ICO_FOLDER"Fill_sel.xpm"
# define FILL_ICO	ICO_FOLDER"Fill.xpm"
# define FINE_SEL_ICO	ICO_FOLDER"Fine_sel.xpm"
# define FINE_ICO	ICO_FOLDER"Fine.xpm"
# define MED_SEL_ICO	ICO_FOLDER"Medium_sel.xpm"
# define MED_ICO	ICO_FOLDER"Medium.xpm"
# define THICK_SEL_ICO	ICO_FOLDER"Thick_sel.xpm"
# define THICK_ICO	ICO_FOLDER"Thick.xpm"

# define LEFT_CLIC	1
# define RIGHT_CLIC	3

# define ABS(x)		((x) < 0) ? (-(x)) : (x)
# define NOZ(x)		((x) == 0) ? ((1)) : (x)

typedef struct	s_ico
{
  int		x;
  int		y;
  char		*unselected;
  char		*selected;
}		t_ico;

typedef struct	s_pp
{
  void		*mlx;
  void		*pwin;
  void		*pimg;
  void		*cwin;
  void		*cimg;
  void		*twin;
  int		pmode;
  int		psize;
  int		pgros;
  int		pfill;
  unsigned int	pcol;
}		t_pp;

typedef struct	s_pt
{
  int		x1;
  int		x2;
  int		y1;
  int		y2;
}		t_pt;

typedef union	u_col
{
  unsigned int	num;
  unsigned char	str[4];
}		t_col;

typedef struct	s_op
{
  int		shape;
  int		(*funct)(int, int, int, t_pp *);
}		t_op;

typedef struct	s_colxy
{
  int		x;
  int		y;
  unsigned int	color;
}		t_colxy;

/*
** hooks/colorbox.c
*/
int		c_expose_hook(t_pp *pp);
int		c_key_hook(int keycode, t_pp *pp);
int		c_mouse_hook(int button, int x, int y, t_pp *pp);

/*
** hooks/main_win.c
*/
int		p_expose_hook(t_pp *pp);
int		p_key_hook(int keycode, t_pp *pp);
int		p_mouse_hook(int button, int x, int y, t_pp *pp);

/*
** hooks/toolbox.c
*/
int		t_expose_hook(t_pp *pp);
int		t_key_hook(int keycode, t_pp *pp);
int		t_mouse_hook(int button, int x, int y, t_pp *pp);

/*
** mlx/init.c
*/
int		init_mlx(t_pp *pp);
unsigned int	get_color(int x, int y);

/*
** mlx/pixel_put.c
*/
int		pixel_put(void *img, int x, int y, unsigned int color);

/*
** mlx/toolbox.c
*/
int		put_toolbox(t_pp *pp);
void		get_tool_mode(int x, int y, t_pp *pp);

/*
** paint/loop.c
*/
int		paint_loop(t_pp *pp);

/*
** paint/shapes/circle.c
*/
int		paint_circle(int button, int x, int y, t_pp *pp);

/*
** paint/shapes/lines.c
*/
int		draw_line(t_pt *xy, t_pp *pp);
int		paint_line(int button, int x, int y, t_pp *pp);

/*
** paint/shapes/shapes.c
*/
int		put_shape(int button, int x, int y, t_pp *pp);

/*
** paint/shapes/square.c
*/
int		paint_square(int button, int x, int y, t_pp *pp);

/*
** paint/shapes/triangle.c
*/
int		paint_triangle(int button, int x, int y, t_pp *pp);

#endif /* !PAINT_H_ */
