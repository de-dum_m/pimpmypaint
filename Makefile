##
## Makefile for pimpmypaint in /home/de-dum_m/code/B2-igraph/pimpmypaint
## 
## Made by de-dum_m
## Login   <de-dum_m@epitech.net>
## 
## Started on  Sat Jun 21 20:18:47 2014 de-dum_m
## Last update Mon Jun 23 12:46:58 2014 de-dum_m
##

NAME	=	pimpmypaint

SRC	=	src/hooks/colorbox.c 		\
		src/hooks/main_win.c 		\
		src/hooks/toolbox.c 		\
		src/mlx/init.c 			\
		src/mlx/pixel_put.c 		\
		src/mlx/toolbox.c 		\
		src/paint/loop.c 		\
		src/paint/shapes/circle.c	\
		src/paint/shapes/lines.c 	\
		src/paint/shapes/shapes.c	\
		src/paint/shapes/square.c	\
		src/paint/shapes/triangle.c	\
		src/pp_main.c

OBJ	=	$(SRC:.c=.o)

CFLAGS	=	-Wall -Wextra -W -pedantic -Iincludes

LDFLAGS	=	-lm -lmlx -lX11 -lXext -lbsd

$(NAME)	:	$(OBJ)
		$(CC) $(OBJ) -o $(NAME) $(LDFLAGS)
		@echo -e "[032mCompiled successfully[0m"

all	:	$(NAME)

clean	:
		rm -f $(OBJ)

fclean	:	clean
		rm -f $(NAME)

re	:	fclean all

.PHONY	:	all re fclean clean
