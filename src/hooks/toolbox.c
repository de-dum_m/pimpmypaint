/*
** toolbox.c for hooks in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Jun 23 12:38:14 2014 de-dum_m
** Last update Mon Jun 23 12:50:15 2014 de-dum_m
*/

#include <stdio.h>
#include <stdlib.h>
#include "paint.h"

int	t_key_hook(int keycode, t_pp *pp)
{
  (void)pp;
  if (keycode == 65307)
    {
      puts("So sad to see you go");
      exit(EXIT_SUCCESS);
    }
  return (SUCCESS);
}

int	t_mouse_hook(int button, int x, int y, t_pp *pp)
{
  (void)button;
  get_tool_mode(x, y, pp);
  return (SUCCESS);
}

int	t_expose_hook(t_pp *pp)
{
  put_toolbox(pp);
  return (SUCCESS);
}
