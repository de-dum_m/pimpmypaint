/*
** main_win.c for hooks in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Jun 23 12:37:05 2014 de-dum_m
** Last update Mon Jun 23 12:42:11 2014 de-dum_m
*/

#include <mlx.h>
#include <stdlib.h>
#include <stdio.h>
#include "paint.h"

int	p_key_hook(int keycode, t_pp *pp)
{
  if (keycode == 65307)
    {
      puts("So sad to see you go");
      exit(EXIT_SUCCESS);
    }
  else if (keycode >= '1' && keycode <= '4')
    pp->pmode = keycode - '1';
  else if (keycode == 65451 && pp->psize < 600)
    ++pp->psize;
  else if (keycode == 65453 && pp->psize > 0)
    --pp->psize;
  put_toolbox(pp);
  return (SUCCESS);
}

int	p_mouse_hook(int button, int x, int y, t_pp *pp)
{
  if (button == 4 && pp->psize < 600)
    pp->psize += 2;
  else if (button == 5 && pp->psize > 0)
    pp->psize -= 2;
  else if (button == 1 || button == 3)
    return (put_shape(button, x, y, pp));
  return (SUCCESS);
}

int	p_expose_hook(t_pp *pp)
{
  mlx_put_image_to_window(pp->mlx, pp->pwin, pp->pimg, 0, 0);
  return (SUCCESS);
}
