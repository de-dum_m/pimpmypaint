/*
** colorbox.c for hooks in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Jun 23 12:37:47 2014 de-dum_m
** Last update Mon Jun 23 12:47:52 2014 de-dum_m
*/

#include <mlx.h>
#include <stdlib.h>
#include <stdio.h>
#include "paint.h"

int	c_key_hook(int keycode, t_pp *pp)
{
  (void)pp;
  if (keycode == 65307)
    {
      puts("So sad to see you go");
      exit(EXIT_SUCCESS);
    }
  return (SUCCESS);
}

int	c_mouse_hook(int button, int x, int y, t_pp *pp)
{
  (void)button;
  pp->pcol = get_color(x, y);
  put_toolbox(pp);
  return (SUCCESS);
}

int	c_expose_hook(t_pp *pp)
{
  mlx_put_image_to_window(pp->mlx, pp->cwin, pp->cimg, 0, 0);
  return (SUCCESS);
}
