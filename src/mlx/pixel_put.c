/*
** pixel_put.c for mlx in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Jun 21 20:44:38 2014 de-dum_m
** Last update Sun Jun 22 18:54:42 2014 de-dum_m
*/

#include <mlx.h>
#include "paint.h"

int	pixel_put(void *img, int x, int y, unsigned int color)
{
  t_col	col;
  char	*data;
  int	bpp;
  int	size_line;
  int	endian;

  if (x > X_PWIN || y > Y_PWIN || x < 0 || y < 0)
    return (SUCCESS);
  if (!(data = mlx_get_data_addr(img, &bpp, &size_line, &endian)))
    return (FAILURE);
  col.num = color;
  x *= 4;
  y *= size_line;
  x += y;
  data[x++] = col.str[0];
  data[x++] = col.str[1];
  data[x++] = col.str[2];
  data[x++] = col.str[3];
  return (SUCCESS);
}
