/*
** toolbox.c for mlx in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Jun 22 16:13:16 2014 de-dum_m
** Last update Mon Jun 23 12:51:58 2014 de-dum_m
*/

#include <mlx.h>
#include <stdlib.h>
#include "paint.h"

static const t_ico	g_ico[] =
  {
    {0, 0, SQUARE_ICO, SQUARE_SEL_ICO},
    {36, 0, CIRCLE_ICO, CIRCLE_SEL_ICO},
    {0, 36, TRIAN_ICO, TRIAN_SEL_ICO},
    {36, 36, LINE_ICO, LINE_SEL_ICO},
    {0, 72, FILL_ICO, FILL_SEL_ICO},
    {0, 110, FINE_ICO, FINE_SEL_ICO},
    {22, 110, MED_ICO, MED_SEL_ICO},
    {44, 110, THICK_ICO, THICK_SEL_ICO},
    {-1, -1, NULL, NULL}
  };

void	get_tool_mode(int x, int y, t_pp *pp)
{
  int	i;
  int	tmpy;

  i = 0;
  while (y - 36 > g_ico[i].y && g_ico[i].y >= 0)
    ++i;
  tmpy = g_ico[i].y;
  while (x > g_ico[i].x && g_ico[i + 1].x >= 0
	 && g_ico[i + 1].x < x && tmpy == g_ico[i + 1].y)
    ++i;
  if (i < 4)
    pp->pmode = i;
  else if (i == 4)
    pp->pfill = ((pp->pfill) == FAILURE) ? SUCCESS : FAILURE;
  else if (i >= 5 && i < 8)
    pp->pgros = ((i - 4)) == 1 ? 1 : (i - 4) * 10;
  put_toolbox(pp);
}

static void	put_color_in_toolbox(int i, t_pp *pp)
{
  int	x;
  int	y;

  x = 18;
  while (x < 36 + 10)
    {
      y = g_ico[i].y + 46;
      while (y < g_ico[i].y + 36 + 36)
	mlx_pixel_put(pp->mlx, pp->twin, x, y++, pp->pcol);
      ++x;
    }
}

int	put_toolbox(t_pp *pp)
{
  int	i;
  void	*tmp_img;
  char	*file;
  int	x;
  int	y;

  i = 0;
  while (g_ico[i].x >= 0)
    {
      if (pp->pmode == i || (i == 4 && pp->pfill == SUCCESS) ||
	  (i >= 5 && (((i - 4)) == 1 ? 1 : (i - 4) * 10) == pp->pgros))
	file = g_ico[i].selected;
      else
	file = g_ico[i].unselected;
      if (!(tmp_img = mlx_xpm_file_to_image(pp->mlx, file,
					    &x, &y)))
	return (FAILURE);
      mlx_put_image_to_window(pp->mlx, pp->twin, tmp_img,
			      g_ico[i].x, g_ico[i].y);
      ++i;
    }
  put_color_in_toolbox(--i, pp);
  return (SUCCESS);
}
