/*
** init.c for mlx in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Jun 21 20:22:01 2014 de-dum_m
** Last update Mon Jun 23 13:54:01 2014 de-dum_m
*/

#include <stdio.h>
#include <mlx.h>
#include "paint.h"

static const t_colxy	g_col[] =
  {
    {0, 0, CBLACK},
    {0, 40, CWHITE},
    {40, 0, CBLUE},
    {40, 40, CCYAN},
    {80, 0, CRED},
    {80, 40, CYELLOW},
    {120, 0, CGREEN},
    {120, 40, CMAGENTA},
    {-1, -1, -1}
  };

static void	put_background(t_pp *pp)
{
  int		x;
  int		y;
  unsigned int	color;

  x = 0;
  color = mlx_get_color_value(pp->mlx, 0xFFFFFF);
  while (x < X_PWIN)
    {
      y = -1;
      while (y < Y_PWIN - 1)
	pixel_put(pp->pimg, x, ++y, color);
      ++x;
    }
}

unsigned int	get_color(int x, int y)
{
  int		i;

  i = 0;
  while (g_col[i].x >= 0 && g_col[i].x < x && x - g_col[i].x > 40)
    ++i;
  if (y > 39)
    ++i;
  return (g_col[i].color);
}

static int	init_colorbox(t_pp *pp)
{
  int		x;
  int		y;

  x = 0;
  while (x < X_CWIN)
    {
      y = 0;
      while (y < Y_CWIN)
	{
	  pixel_put(pp->cimg, x, y, get_color(x, y));
	  ++y;
	}
      ++x;
    }
  mlx_put_image_to_window(pp->mlx, pp->cwin, pp->cimg, 0, 0);
  return (SUCCESS);
}

int	init_mlx(t_pp *pp)
{
  if (!(pp->mlx = mlx_init())
      || !(pp->pwin = mlx_new_window(pp->mlx, X_PWIN, Y_PWIN, "pimpmypaint"))
      || !(pp->pimg = mlx_new_image(pp->mlx, X_PWIN, Y_PWIN))
      || !(pp->cwin = mlx_new_window(pp->mlx, X_CWIN, Y_CWIN, "colorbox"))
      || !(pp->cimg = mlx_new_image(pp->mlx, X_CWIN, Y_CWIN))
      || !(pp->twin = mlx_new_window(pp->mlx, X_TWIN, Y_TWIN, "toolbox")))
    {
      fputs("pimpmypaint: mlx init failed\n", stderr);
      return (FAILURE);
    }
  put_background(pp);
  pp->pmode = P_SQUARE;
  pp->pcol = mlx_get_color_value(pp->mlx, 0x1793D1);
  pp->psize = DEFAULT_SIZE;
  pp->pgros = 20;
  pp->pfill = FAILURE;
  init_colorbox(pp);
  if (put_toolbox(pp) == FAILURE)
    {
      fputs("Creating toolbox failed\n", stderr);
      return (FAILURE);
    }
  return (SUCCESS);
}
