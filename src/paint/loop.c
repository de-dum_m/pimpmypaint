/*
** loop.c for paint in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Jun 21 20:38:32 2014 de-dum_m
** Last update Mon Jun 23 12:50:41 2014 de-dum_m
*/

#include <mlx.h>
#include <stdlib.h>
#include <stdio.h>
#include "paint.h"

int	paint_loop(t_pp *pp)
{
  mlx_expose_hook(pp->pwin, p_expose_hook, pp);
  mlx_expose_hook(pp->cwin, c_expose_hook, pp);
  mlx_expose_hook(pp->twin, t_expose_hook, pp);
  mlx_key_hook(pp->pwin, p_key_hook, pp);
  mlx_key_hook(pp->cwin, c_key_hook, pp);
  mlx_key_hook(pp->twin, t_key_hook, pp);
  mlx_mouse_hook(pp->pwin, p_mouse_hook, pp);
  mlx_mouse_hook(pp->cwin, c_mouse_hook, pp);
  mlx_mouse_hook(pp->twin, t_mouse_hook, pp);
  mlx_loop(pp->mlx);
  return (SUCCESS);
}
