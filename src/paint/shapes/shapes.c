/*
** shapes.c for shapes in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Jun 22 10:39:51 2014 de-dum_m
** Last update Mon Jun 23 13:12:53 2014 de-dum_m
*/

#include <mlx.h>
#include "paint.h"

static const t_op	g_op[] =
  {
    {P_SQUARE, &paint_square},
    {P_CIRCLE, &paint_circle},
    {P_TRIANGLE, &paint_triangle},
    {P_LINE, &paint_line},
    {-1, (void *)0}
  };

int	put_shape(int button, int x, int y, t_pp *pp)
{
  int	i;
  int	j;
  int	tmp_size;

  i = 0;
  j = 0;
  while (pp->pmode != g_op[i].shape && g_op[i].shape >= 0)
    ++i;
  if (pp->pfill == SUCCESS && i != P_LINE)
    j = pp->psize;
  else
    j = pp->pgros;
  tmp_size = pp->psize;
  if (g_op[i].shape >= 0)
    while (j > 0)
      {
	if (g_op[i].funct(button, x, y, pp) == FAILURE)
	  return (FAILURE);
	--pp->psize;
	--j;
      }
  mlx_put_image_to_window(pp->mlx, pp->pwin, pp->pimg, 0, 0);
  pp->psize = tmp_size;
  return (SUCCESS);
}
