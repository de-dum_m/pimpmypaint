/*
** square.c for shapes in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Jun 22 12:11:34 2014 de-dum_m
** Last update Mon Jun 23 12:49:46 2014 de-dum_m
*/

#include "paint.h"

int	paint_square(int button, int x, int y, t_pp *pp)
{
  t_pt	xy;

  (void)button;
  xy.x1 = x - pp->psize / 2;
  xy.y1 = y - pp->psize / 2;
  xy.x2 = x + pp->psize / 2;
  xy.y2 = xy.y1;
  draw_line(&xy, pp);
  xy.x2 = xy.x1;
  xy.y2 = xy.y1 + pp->psize;
  draw_line(&xy, pp);
  xy.x1 = xy.x2 + pp->psize;
  xy.y1 = xy.y2;
  draw_line(&xy, pp);
  xy.x1 += pp->psize;
  xy.x2 = xy.x1;
  xy.y2 = xy.y1 - pp->psize;
  draw_line(&xy, pp);
  return (SUCCESS);
}
