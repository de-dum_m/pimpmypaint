/*
** lines.c for shapes in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Jun 22 11:22:18 2014 de-dum_m
** Last update Mon Jun 23 13:51:38 2014 de-dum_m
*/

#include "paint.h"

static void	swap_pos(t_pt *xy)
{
  int		tmp_x;
  int		tmp_y;

  tmp_x = xy->x1;
  tmp_y = xy->y1;
  xy->x1 = xy->x2;
  xy->y1 = xy->y2;
  xy->x2 = tmp_x;
  xy->y2 = tmp_y;
}

static int	draw_line_one(t_pt *xy, t_pp *pp)
{
  int		x;

  if (xy->x1 > xy->x2)
    swap_pos(xy);
  x = xy->x1;
  while (x <= xy->x2)
    {
      if (pixel_put(pp->pimg, x,
		    xy->y1 + ((xy->y2 - xy->y1)
			      * (x - xy->x1)) / (xy->x2 - xy->x1),
		    pp->pcol) == FAILURE)
	return (FAILURE);
      ++x;
    }
  return (SUCCESS);
}

static int	draw_line_two(t_pt *xy, t_pp *pp)
{
  int		y;

  if (xy->y1 > xy->y2)
    swap_pos(xy);
  y = xy->y1;
  while (y <= xy->y2)
    {
      if (pixel_put(pp->pimg,
		    xy->x1 + ((xy->x2 - xy->x1)
			      * (y - xy->y1)) / (xy->y2 - xy->y1),
		    y,
		    pp->pcol) == FAILURE)
	return (FAILURE);
      ++y;
    }
  return (SUCCESS);
}

int	draw_line(t_pt *xy, t_pp *pp)
{
  int	a;
  int	b;

  if (xy->x1 == xy->x2 && xy->y1 == xy->y2)
    return (FAILURE);
  if ((a = ABS(xy->x2 - xy->x1)) >= (b = ABS(xy->y2 - xy->y1)))
    draw_line_one(xy, pp);
  else
    draw_line_two(xy, pp);
  return (SUCCESS);
}

int		paint_line(int button, int x, int y, t_pp *pp)
{
  static int	done;
  static t_pt	xy;

  (void)button;
  if (!done)
    {
      xy.x1 = x;
      xy.y1 = y;
      ++done;
      return (SUCCESS);
    }
  else
    {
      xy.x2 = x;
      xy.y2 = y;
      done = 0;
    }
  draw_line(&xy, pp);
  return (SUCCESS);
}
