/*
** circle.c for shapes in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Jun 22 10:38:53 2014 de-dum_m
** Last update Mon Jun 23 12:48:46 2014 de-dum_m
*/

#include <math.h>
#include "paint.h"

int		paint_circle(int button, int x, int y, t_pp *pp)
{
  double	a;

  a = 0;
  (void)button;
  while (a <= 360)
    {
      if (pixel_put(pp->pimg, x + (pp->psize / 2) * cos(a),
		    y + (pp->psize / 2) * sin(a), pp->pcol) == FAILURE)
	return (FAILURE);
      a += ((2 * M_PI) / (4 * pp->psize));
    }
  return (SUCCESS);
}
