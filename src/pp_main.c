/*
** pp_main.c for src in /home/de-dum_m/code/B2-igraph/pimpmypaint
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Jun 21 20:17:54 2014 de-dum_m
** Last update Mon Jun 23 13:50:50 2014 de-dum_m
*/

#include "paint.h"

int	main(void)
{
  t_pp	pp;

  if (init_mlx(&(pp)) == FAILURE)
    return (FAILURE);
  paint_loop(&pp);
  return (SUCCESS);
}
